import os


def main():
    os.system("mvn compile")
    os.system("mvn package -DskipTests")
    os.system("docker build -t demo:1.0 .")


if __name__ == '__main__':
    main()
