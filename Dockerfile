FROM openjdk:11
RUN mkdir -p /home/app
COPY ./target/demo-0.0.1-SNAPSHOT.jar home/app/demo.jar
CMD ["java","-jar","/home/app/demo.jar"]
