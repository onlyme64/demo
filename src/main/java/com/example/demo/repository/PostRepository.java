package com.example.demo.repository;

import java.time.LocalDate;

import com.example.demo.entities.Post;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    Page<Post> findAll(Pageable pageable);

    Page<Post> findByAuthorName(String authorName, Pageable pageable);

    Page<Post> findByCreatedDate(LocalDate createdDate, Pageable pageable);

    Page<Post> findByAuthorNameAndCreatedDate(String authorName, LocalDate createdDate, Pageable pageable);
}
