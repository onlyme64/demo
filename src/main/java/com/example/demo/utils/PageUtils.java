package com.example.demo.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class PageUtils {
    public Pageable getPageable(int size, int page) {
        size = size == 0 ? 10 : size;
        return PageRequest.of(page, size);
    }
}
