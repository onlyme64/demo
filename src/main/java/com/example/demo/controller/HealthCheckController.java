package com.example.demo.controller;

import com.example.demo.services.IHealthCheckService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo/api/v1/healthcheck")
public class HealthCheckController {

    @Autowired
    IHealthCheckService healthCheckService;

    @GetMapping("/")
    public ResponseEntity<Object> check() {
        return ResponseEntity.ok(healthCheckService.healthCheck());
    }

}
