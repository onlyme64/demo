package com.example.demo.controller;

import java.time.LocalDate;

import com.example.demo.dtos.ExceptionDto;
import com.example.demo.dtos.PostCreationDto;
import com.example.demo.entities.Post;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.services.IPostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo/api/v1/post")
@CrossOrigin
public class PostController {

    @Autowired
    IPostService postService;

    @PostMapping(value = "/")
    public ResponseEntity<Object> createPost(@RequestBody PostCreationDto dto) {
        Post createdPost = postService.createPost(dto);
        return ResponseEntity.ok(createdPost);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getPost(@PathVariable("id") Long id) {
        Post post = postService.getPost(id);
        return ResponseEntity.ok(post);
    }

    @GetMapping(value = "/")
    public ResponseEntity<Object> find(@RequestParam(name = "author", required = false) String authorName,
            @RequestParam(name = "creationtime", required = false) LocalDate createdDate,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "0") int page) {
        Page<Post> result;
        if (authorName == null && createdDate == null) {
            result = postService.getAll(page, size);
        } else if (authorName != null && createdDate != null) {
            result = postService.findByAuthorNameAndCreatedDate(authorName, createdDate, page, size);
        } else if (authorName != null) {
            result = postService.findByAuthorName(authorName, page, size);
        } else {
            result = postService.findBycreatedDate(createdDate, page, size);
        }
        return ResponseEntity.ok(result);
    }

    @ExceptionHandler({ ResourceNotFoundException.class })
    public ResponseEntity<Object> handler() {
        return ResponseEntity.status(404).body(new ExceptionDto(404, "Post not found."));
    }
}
