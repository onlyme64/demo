package com.example.demo.services;

import java.time.LocalDate;

import com.example.demo.dtos.PostCreationDto;
import com.example.demo.entities.Post;

import org.springframework.data.domain.Page;

public interface IPostService {
    Post createPost(PostCreationDto creationDto);

    Page<Post> getAll(int page, int size);

    Post getPost(Long id);

    Page<Post> findByAuthorName(String authorName, int page, int size);

    Page<Post> findBycreatedDate(LocalDate createdDate, int page, int size);

    Page<Post> findByAuthorNameAndCreatedDate(String authorName, LocalDate createdDate, int page, int size);
}
