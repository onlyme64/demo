package com.example.demo.services;

public interface IHealthCheckService {
    String healthCheck();
}
