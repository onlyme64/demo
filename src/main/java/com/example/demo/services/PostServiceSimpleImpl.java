package com.example.demo.services;

import java.time.LocalDate;

import com.example.demo.dtos.PostCreationDto;
import com.example.demo.entities.Post;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repository.PostRepository;
import com.example.demo.utils.PageUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PostServiceSimpleImpl implements IPostService {

    @Autowired
    PostRepository postRepository;
    @Autowired
    PageUtils pageUtils;

    @Override
    public Post createPost(PostCreationDto creationDto) {
        Post createdPost = new Post(creationDto);
        createdPost = postRepository.save(createdPost);
        return createdPost;
    }

    @Override
    public Page<Post> getAll(int page, int size) {
        Pageable pageable = pageUtils.getPageable(size, page);
        return postRepository.findAll(pageable);
    }

    @Override
    public Post getPost(Long id) {
        return postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public Page<Post> findByAuthorName(String authorName, int page, int size) {
        Pageable pageable = pageUtils.getPageable(size, page);
        return postRepository.findByAuthorName(authorName, pageable);
    }

    @Override
    public Page<Post> findBycreatedDate(LocalDate createdDate, int page, int size) {
        Pageable pageable = pageUtils.getPageable(size, page);
        return postRepository.findByCreatedDate(createdDate, pageable);
    }

    @Override
    public Page<Post> findByAuthorNameAndCreatedDate(String authorName, LocalDate createdDate, int page, int size) {
        Pageable pageable = pageUtils.getPageable(size, page);
        return postRepository.findByAuthorNameAndCreatedDate(authorName, createdDate, pageable);
    }
}
