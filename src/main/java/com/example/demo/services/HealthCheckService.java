package com.example.demo.services;

import org.springframework.stereotype.Service;

@Service
public class HealthCheckService implements IHealthCheckService {

    @Override
    public String healthCheck() {
        return "Application is running...";
    }

}
