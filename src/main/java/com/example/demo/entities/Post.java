package com.example.demo.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.example.demo.dtos.PostCreationDto;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "post")
@Table(name = "POST")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "author", nullable = true, length = 30)
    @Size(min = 6)
    @NotBlank
    private String authorName;
    @Column(name = "content", nullable = false, length = 65535)
    @Size(min = 50)
    @NotBlank
    private String content;
    @CreationTimestamp
    private LocalDate createdDate;

    public Post() {
    }

    public Post(PostCreationDto postCreationDto) {
        this.authorName = postCreationDto.getAuthorName();
        this.content = postCreationDto.getContent();
    }

    public Post(String authorName, String content) {
        this.authorName = authorName;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

}
